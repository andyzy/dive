package com.example.divesb.annotation;


import com.example.divesb.configuration.HelloWorldConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({HelloWorldConfiguration.class})
public @interface EnableHelloWorld {

}
