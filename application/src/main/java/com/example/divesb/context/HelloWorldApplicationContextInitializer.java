package com.example.divesb.context;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.annotation.Order;

@Order(Integer.MIN_VALUE)
public class HelloWorldApplicationContextInitializer<C extends ConfigurableApplicationContext>   implements ApplicationContextInitializer<C> {

    @Override
    public void initialize(C applicationContext) {
        System.out.println("HelloWorldApplicationContextInitializer.id"+applicationContext.getId());

    }
}
