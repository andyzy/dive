package com.example.divesb.configuration;


import com.example.divesb.annotation.EnableHelloWorld;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableHelloWorld
public class HelloWorldAutoConfiguration {

}
