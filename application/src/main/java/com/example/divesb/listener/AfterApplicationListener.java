package com.example.divesb.listener;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.annotation.Order;

@Order(Integer.MAX_VALUE)
public class AfterApplicationListener implements ApplicationListener<ContextRefreshedEvent> {
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        System.out.println("AfterApplicationListener.id"+event.getApplicationContext().getId()+" timestamp"+event.getTimestamp());
    }
}
