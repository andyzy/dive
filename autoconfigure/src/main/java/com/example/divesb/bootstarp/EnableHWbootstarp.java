package com.example.divesb.bootstarp;


import com.example.divesb.annotation.EnableHelloWorld;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

@EnableHelloWorld
public class EnableHWbootstarp {

    public static void main(String[] args){
        ConfigurableApplicationContext configurableApplicationContext =
                new SpringApplicationBuilder(EnableHWbootstarp.class)
                        .web(WebApplicationType.NONE)
                        .run(args);

      String helloWorld =  configurableApplicationContext.getBean("helloWorld",String.class);
      System.out.println("hello world bean :"+helloWorld);

        configurableApplicationContext.close();


    }



}
