package com.example.divesb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class SbApplicationBootstarp {

    public static void main(String[] args) {


        new SpringApplicationBuilder(SbApplicationBootstarp.class)
                .web(WebApplicationType.SERVLET)
                .run(args);

    }



}
