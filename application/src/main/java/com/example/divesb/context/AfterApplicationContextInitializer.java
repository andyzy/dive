package com.example.divesb.context;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.annotation.Order;

@Order
public class AfterApplicationContextInitializer  implements ApplicationContextInitializer {

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        System.out.println("AfterApplicationContextInitializer.id"+applicationContext.getId());

    }
}
