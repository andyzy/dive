package com.example.divesb;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class SpringApplicationContextBootstarp {

    public static void main(String[] args) {
        ConfigurableApplicationContext context =  new SpringApplicationBuilder(SpringApplicationContextBootstarp.class)
                .run(args);
        System.out.println("ConfigurableApplicationContext"+context.getClass().getName());
        context.close();
    }
}
