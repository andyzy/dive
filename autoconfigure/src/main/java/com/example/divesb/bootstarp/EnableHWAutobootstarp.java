package com.example.divesb.bootstarp;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

@EnableAutoConfiguration
public class EnableHWAutobootstarp {
    public static void main(String[] args) {
        ConfigurableApplicationContext configurableApplicationContext =
                new SpringApplicationBuilder(EnableHWAutobootstarp.class)
                        .web(WebApplicationType.NONE)
                        .run(args);

        String helloWorld = configurableApplicationContext.getBean("helloWorld", String.class);
        System.out.println("hello world bean :" + helloWorld);

        configurableApplicationContext.close();
    }
}
